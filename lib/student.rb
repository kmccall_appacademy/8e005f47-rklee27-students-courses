class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    raise if @courses.any? { |courses| courses.conflicts_with?(course) }
    @courses << course if @courses.include?(course) == false
    course.students << self
  end

  def course_load
    hash = Hash.new(0)
    self.courses.each do |course|
      hash[course.department] += course.credits
    end

    hash
  end

end
